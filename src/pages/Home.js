import React, { useContext, useState } from "react";
import { UserContext } from "../context/UserContext";
import { toast } from "react-toastify";
import Axios from "axios";
import UserCard from "../components/UserCard";
import Repos from "../components/Repos";
import { Navigate } from "react-router-dom";

import "./home.css"

const Home = () => {
  const context = useContext(UserContext);
  const [query, setQuery] = useState("");
  const [user, setUser] = useState(null);

  const fetchDetails = async () => {
    try {
      const { data } = await Axios.get(`https://api.github.com/users/${query}`);
      setUser(data);
      console.log(data);
    } catch (error) {
      toast("Not able to locate user", { type: "error" });
    }
  };

  if (!context.user?.uid) {
    return <Navigate replace to="/signin" />;
  }

  return (
    <section>
      <div className="input-group mb-3 mt-2 container" style={{width:"300px", margin:"0 auto"}}>
        <input
          type="text"
          className="form-control"
          value={query}
          placeholder="Please Provide user name"
          onChange={(e) => setQuery(e.target.value)}
        />
        <button className="btn btn-primary" onClick={fetchDetails}>
          Fetch User
        </button>
      </div>
      <div className="container border border-2 rounded w-auto">
        <div className="row m-3 grid-container" style={{ height: "67vh" }}>
          <div className="col-5">{user ? <UserCard user={user} /> : null}</div>
          <div className="col">
            {user ? <Repos repos_url={user.repos_url} /> : null}
          </div>
        </div>
      </div>
    </section>
  );
};

export default Home;
