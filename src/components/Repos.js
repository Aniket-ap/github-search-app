import React, { useState, useEffect } from "react";
import Axios from "axios";
import "./repos.css";
import { Link } from "react-router-dom";

const Repos = ({ repos_url }) => {
  const [repos, setRepos] = useState([]);
  const fetchRepos = async () => {
    const { data } = await Axios.get(repos_url);
    setRepos(data);
  };

  useEffect(() => {
    fetchRepos();
  }, [repos_url]);

  return (
    <div className="repos-container">
      {repos.map((repo) => (
        <div key={repo.id}>
          <div className="card-body">
            <p>
              {repo.name} ||{" "}
              <span>
                <a href={repo.html_url}>Visit Repo</a>
              </span>
            </p>
            <p>Language : {repo.language}</p>
          </div>
        </div>
      ))}
    </div>
  );
};

export default Repos;
