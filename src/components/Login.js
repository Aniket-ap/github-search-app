import React from 'react'
import "./login.css"

const Login = ({handleSubmit, email, setEmail, password, setPassword, title}) => {
  return (
    <div className="form-container">
        <h1>{title}</h1>
        <form onSubmit={handleSubmit}>
          <div className="control">
            <label for="email">Email</label>
            <input
              type="email"
              name="email"
              placeholder="provide your email"
              value={email}
              onChange={(e) => setEmail(e.target.value)}
            />
          </div>
          <div className="control">
            <label for="password">Password</label>

            <input
              type="password"
              name="password"
              placeholder="provide your password"
              value={password}
              onChange={(e) => setPassword(e.target.value)}
            />
          </div>
          <button className="btn btn-primary" type="submit">{title}</button>
        </form>
      </div>
  )
}

export default Login