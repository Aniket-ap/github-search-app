import React from "react";
import "./userCard.css";

import { GoLocation } from "react-icons/go";
import { FaGithub } from "react-icons/fa";

const UserCard = ({ user }) => {
  return (
    <div className="card-container">
      <div className="card">
        <div className="avatar">
          <img src={user.avatar_url} alt="img" />
        </div>
        <div className="title">
          <h2>{user.name}</h2>
        </div>
        <p>
          <GoLocation /> {user.location}
        </p>
        <div className="description">{user.bio ? user.bio : null}</div>
        <div className="follow">
          <div>
            <p>Followers</p>
            <h5>{user.followers}</h5>
          </div>
          <div>
            <p>Following</p>
            <h5>{user.following}</h5>
          </div>
          <div>
            <p>Repos</p>
            <h5>{user.public_repos}</h5>
          </div>
        </div>
        <div className="social">
          <FaGithub size={30} />
        </div>
      </div>
    </div>
  );
};

export default UserCard;
